package bf.ouily.ribbala.controlleur;

import bf.ouily.ribbala.model.Client;

public class ClientControl {

    private static ClientControl clientControl = null;
    private Client client;
    private ClientControl(){}

    public static ClientControl getInstance(){
        if (clientControl == null){
            clientControl = new ClientControl();
        }

        return clientControl;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
