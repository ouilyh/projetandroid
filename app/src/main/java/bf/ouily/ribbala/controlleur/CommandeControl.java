package bf.ouily.ribbala.controlleur;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;

import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Commande;

public class CommandeControl {


    private ArrayList<Commande> commandes = new ArrayList<Commande>();
    private static CommandeControl commandeControl = null;
    private Context context;
    private AccessLocal accessLocal;

    private CommandeControl(){

    }

    public static CommandeControl getInstance(){

        if( commandeControl == null){
            commandeControl = new CommandeControl();
        }
        return commandeControl;
    }


    public ArrayList<Commande> getCommandes() {
        accessLocal = new AccessLocal(context);
        commandes = accessLocal.getCommandeDAO().select();
        if (commandes == null) commandes = new ArrayList<Commande>();
        else {
            for (int i = 0; i < commandes.size(); i++){

                commandes.get(i).setPlatCommanders(
                        accessLocal.getPlatCommaderDAO()
                                .selectByCommandeId(
                                        commandes.get(i)
                                )
                );
            }
        }

        return commandes;
    }

    public void setCommandes(ArrayList<Commande> commandes) {
        this.commandes = commandes;
    }

    public static CommandeControl getCommandeControl() {
        return commandeControl;
    }

    public static void setCommandeControl(CommandeControl commandeControl) {
        CommandeControl.commandeControl = commandeControl;
    }

    public void addCommande(Commande commande){
        accessLocal = new AccessLocal(context);
        int id = accessLocal.getCommandeDAO().createId(commande);
        commande.setId(id);
        for (int i = 0; i< commande.getPlatCommanders().size(); i++){
            commande.getPlatCommanders().get(i).setCommande(commande);
            accessLocal.getPlatCommaderDAO().create(commande.getPlatCommanders().get(i));
        }
        this.commandes.add(commande);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
