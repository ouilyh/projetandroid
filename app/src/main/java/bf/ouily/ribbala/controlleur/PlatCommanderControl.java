package bf.ouily.ribbala.controlleur;

import android.content.Context;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Observer;

import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.model.PlatCommander;
import bf.ouily.ribbala.outil.Observable;

public class PlatCommanderControl{

    private Plat plat;
    private int commande;
    private int quantite;

    private int somme = 0;


    private ArrayList<PlatCommander> platCommanders = new ArrayList<PlatCommander>();
    private static PlatCommanderControl platCommanderControl = null;
    private AccessLocal accessLocal;
    private RestaurantControl restaurantControl = RestaurantControl.getInstance();
    private Context context;

    private PlatCommanderControl(){
        this.somme = 0;
    }

    public static PlatCommanderControl getInstance(){
        if (platCommanderControl == null){
            platCommanderControl = new PlatCommanderControl();
        }
        return platCommanderControl;
    }

    public Plat getPlat() {
        return plat;
    }

    public void setPlat(Plat plat) {
        this.plat = plat;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public ArrayList<PlatCommander> getPlatCommanders() {
        return platCommanders;
    }

    public void setPlatCommanders(ArrayList<PlatCommander> platCommanders) {
        this.platCommanders = platCommanders;
    }

    public int getSomme() {
        return somme;
    }

    public void setSomme(int somme) {
        this.somme = somme;

    }

    public void addPlatCommander(PlatCommander platCommander){
        this.platCommanders.add(platCommander);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public  static void changeIntance(){
        platCommanderControl = null;
    }

    public ArrayList<PlatCommander> getPlatCommandersRestaurant() {
        accessLocal = new AccessLocal(context);
        return accessLocal.getPlatCommaderDAO().selectByRestaurantId(restaurantControl.getRestaurant());

    }
}
