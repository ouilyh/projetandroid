package bf.ouily.ribbala.controlleur;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;

import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.model.Restaurant;
import bf.ouily.ribbala.model.User;

public class PlatControl implements Serializable {

    // Proprietes

    private AccessLocal local;

    private String nomPlat;
    private String desc;
    private String img;
    private int prix;
    private int type;
    private String nomResto;

    private Plat plat;

    private User user;
    private Context context;

    private ArrayList<Plat> listPlat = new ArrayList<Plat>();

    private static PlatControl platControl = null;

    private PlatControl(){
        construction();

    }

    public static PlatControl getInstance(){
        if (platControl == null)
            platControl = new PlatControl();
        return platControl;
    }

    private void construction(){
        nomPlat = "Menu Chicken";
        desc = "Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise + Frites + Boisson";
        img = "13.jpg";
        prix = 200;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Double Steak";
        desc = "Sandwich: Double Burger, Fromage, Bacon, Salade, Tomate + Frites + Boisson";
        img = "b1.png";
        prix = 150;
        type = 1;

        plat = new Plat(nomPlat,desc,img,prix,type);
        listPlat.add(plat);

        nomPlat = "Classic";
        desc = "Sandwich: Burger, Salade, Tomate, Cornichon";
        img = "b1.png";
        prix = 350;
        type = 2;
        nomResto = "Restaurant Farouch";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Bacon";
        desc = "Sandwich: Burger, Fromage, Bacon, Salade, Tomate";
        img = "b1.png";
        prix = 100;
        type = 2;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Double Steak";
        desc = "Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise";
        img = "b1.png";
        prix = 1750;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);


        nomPlat = "Menu Chicken";
        desc = "Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise + Frites + Boisson";
        img = "13.jpg";
        prix = 100;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Double Steak";
        desc = "Sandwich: Double Burger, Fromage, Bacon, Salade, Tomate + Frites + Boisson";
        img = "b1.png";
        prix = 500;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Classic";
        desc = "Sandwich: Burger, Salade, Tomate, Cornichon";
        img = "b1.png";
        prix = 250;
        type = 2;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Bacon";
        desc = "Sandwich: Burger, Fromage, Bacon, Salade, Tomate";
        img = "b1.png";
        prix = 900;
        type = 1;
        nomResto = "Restaurant Farouch";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Double Steak";
        desc = "Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise";
        img = "b1.png";
        prix = 800;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Chicken";
        desc = "Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise + Frites + Boisson";
        img = "13.jpg";
        prix = 700;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Double Steak";
        desc = "Sandwich: Double Burger, Fromage, Bacon, Salade, Tomate + Frites + Boisson";
        img = "b1.png";
        prix = 600;
        type = 1;
        nomResto = "Restaurant Farouch";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Classic";
        desc = "Sandwich: Burger, Salade, Tomate, Cornichon";
        img = "b1.png";
        prix = 500;
        type = 1;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Bacon";
        desc = "Sandwich: Burger, Fromage, Bacon, Salade, Tomate";
        img = "b1.png";
        prix = 750;
        type = 2;
        nomResto = "Restaurant Aldo";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);

        nomPlat = "Menu Double Steak";
        desc = "Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise";
        img = "b1.png";
        prix = 850;
        type = 2;
        nomResto = "Restaurant Farouch";

        plat = new Plat(nomPlat,desc,img,prix,type,nomResto);
        listPlat.add(plat);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ArrayList<Plat> getListPlatRestaurant() {

        local = new AccessLocal(context);

        if (UserControl.getInstance().getUser().getTypeUser() == 2)
            listPlat = local.getPlatDAO().selectById(RestaurantControl.getInstance(context).getRestaurant());
        else listPlat = local.getPlatDAO().select();

        return listPlat;
    }
    public ArrayList<Plat> getListPlat() {

        local = new AccessLocal(context);
        listPlat = local.getPlatDAO().select();

        return listPlat;
    }

    public void setListPlat(ArrayList<Plat> listPlat) {
        this.listPlat = listPlat;
    }

    public void addPlat(Plat plat, Context context){
        local = new AccessLocal(context);
        local.getPlatDAO().create(plat);
        listPlat.add(plat);
    }

    public void updatePlat(Plat plat, Context context){
        local = new AccessLocal(context);
        local.getPlatDAO().update(plat);


    }
    public void delPlat(Plat plat, Context context){
        local = new AccessLocal(context);
        local.getPlatDAO().delete(plat);

    }
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
