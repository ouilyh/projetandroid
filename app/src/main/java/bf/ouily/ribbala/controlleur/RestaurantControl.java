package bf.ouily.ribbala.controlleur;

import android.content.Context;

import java.util.ArrayList;

import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Restaurant;
import bf.ouily.ribbala.model.User;

public class RestaurantControl {

    private int id;
    private String nomResto = "Restaurant Aldo";
    private String adresse;
    private String mail;

    private User user = null;

    private Restaurant restaurant;

    private static RestaurantControl control;

    private ArrayList<Restaurant> list = new ArrayList<Restaurant>();

    private AccessLocal local;

    private RestaurantControl(Context context){

        local = new AccessLocal(context);
    }
    private RestaurantControl(){
    }

    public static RestaurantControl getInstance(Context context){
        if (control == null)
            control = new RestaurantControl(context);
        return control;
    }
    public static RestaurantControl getInstance(){
        if (control == null)
            control = new RestaurantControl();
        return control;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomResto() {
        return nomResto;
    }

    public void setNomResto(String nomResto) {
        this.nomResto = nomResto;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public User getUser() {
        return user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Restaurant> getList() {
        list = local.getRestaurantDAO().select();
        return list;
    }

    public void setList(ArrayList<Restaurant> list) {
        this.list = list;
    }

    public void addRestaurant(Restaurant restaurant){
        list.add(restaurant);
    }

    public void delRestauant(int i){
        local.getRestaurantDAO().delete(list.get(i));
        list.remove(i);
    }
}
