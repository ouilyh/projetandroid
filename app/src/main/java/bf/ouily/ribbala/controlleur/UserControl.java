package bf.ouily.ribbala.controlleur;

import bf.ouily.ribbala.model.User;

public class UserControl {

    private User user;

    private static UserControl userControl = null;

    public static UserControl getInstance(){
        if (userControl == null) userControl = new UserControl();
        return userControl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
