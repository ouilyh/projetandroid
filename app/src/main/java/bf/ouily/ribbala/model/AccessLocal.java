package bf.ouily.ribbala.model;

import android.content.Context;

import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class AccessLocal {

    private MyDatabaseOpenHelper accesDB;

    private UserDAO userDAO;
    private ClientDAO clientDAO;
    private RestaurantDAO restaurantDAO;
    private PlatDAO platDAO;
    private CommandeDAO commandeDAO;
    private PlatCommaderDAO platCommaderDAO;

    public AccessLocal(Context context){
        accesDB         = new MyDatabaseOpenHelper(context);
        userDAO         = new UserDAO(accesDB);
        clientDAO       = new ClientDAO(accesDB);
        restaurantDAO   = new RestaurantDAO(accesDB);
        platDAO         = new PlatDAO(accesDB);
        commandeDAO     = new CommandeDAO(accesDB);
        platCommaderDAO = new PlatCommaderDAO(accesDB);
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public ClientDAO getClientDAO() {
        return clientDAO;
    }

    public RestaurantDAO getRestaurantDAO() {
        return restaurantDAO;
    }

    public void setRestaurantDAO(RestaurantDAO restaurantDAO) {
        this.restaurantDAO = restaurantDAO;
    }

    public PlatDAO getPlatDAO() {
        return platDAO;
    }

    public void setPlatDAO(PlatDAO platDAO) {
        this.platDAO = platDAO;
    }

    public CommandeDAO getCommandeDAO() {
        return commandeDAO;
    }

    public void setCommandeDAO(CommandeDAO commandeDAO) {
        this.commandeDAO = commandeDAO;
    }

    public PlatCommaderDAO getPlatCommaderDAO() {
        return platCommaderDAO;
    }

    public void setPlatCommaderDAO(PlatCommaderDAO platCommaderDAO) {
        this.platCommaderDAO = platCommaderDAO;
    }
}
