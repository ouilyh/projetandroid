package bf.ouily.ribbala.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class ClientDAO extends DAO<Client> {

    private SQLiteDatabase db;

    private DAO<User> userDAO = null;

    public ClientDAO(MyDatabaseOpenHelper conn) {
        super(conn);
        userDAO = new UserDAO(conn);
    }

    /**
     * Méthode de création
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean create(Client obj) {

        db = connect.getWritableDatabase();
        String req = "insert into clients (nomClient,prenomClient,adresse, numero,email,user_id) values";
        req += "('" +obj.getNomClient()+
                "','" +obj.getPrenomClient()+
                "','" +obj.getAdresse()+
                "','" +obj.getNumTel()+
                "','" +obj.getEmail()+
                "','" +obj.getUser().getId()+
                "')";
        db.execSQL(req);
        return false;
    }

    @Override
    public ArrayList<Client> select() {
        db = connect.getReadableDatabase();
        ArrayList<Client> clients = new ArrayList<Client>();
        String selectQuery = "SELECT  * FROM clients";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                Client client = new Client();
                client.setId(Integer.parseInt(cursor.getString(0)));
                client.setNomClient(cursor.getString(1));
                client.setPrenomClient(cursor.getString(2));
                client.setAdresse(cursor.getString(3));
                client.setNumTel(cursor.getString(4));
                client.setEmail(cursor.getString(5));
                client.setUser(userDAO.find(cursor.getInt(6)));

                // Ajouter a la liste
                clients.add(client);
            } while (cursor.moveToNext());
        }

        return clients;
    }

    /**
     * Méthode pour effacer
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean delete(Client obj) {
        return false;
    }

    /**
     * Méthode de mise à jour
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean update(Client obj) {
        return false;
    }

    /**
     * Méthode de recherche des informations
     *
     * @param id
     * @return T
     */
    @Override
    public Client find(int id) {
        return null;
    }
    public Client findUserId(int id) {

        Client client = new Client();

        db = connect.getReadableDatabase();
        String req = "SELECT * FROM clients WHERE user_id = "+id;

        Cursor cursor = db.rawQuery(req, null);
        cursor.moveToLast();
        if (!cursor.isAfterLast()){

            client.setId(Integer.parseInt(cursor.getString(0)));
            client.setNomClient(cursor.getString(1));
            client.setPrenomClient(cursor.getString(2));
            client.setAdresse(cursor.getString(3));
            client.setNumTel(cursor.getString(4));
            client.setEmail(cursor.getString(5));
            client.setUser(userDAO.find(cursor.getInt(6)));

        }
        return client;
    }
}
