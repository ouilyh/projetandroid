package bf.ouily.ribbala.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Commande implements Serializable {

    private int id;
    private int somme;
    private Date date;
    private Boolean livraison;
    private boolean etat = true;
    private boolean etat2 = false;
    private ArrayList<PlatCommander> platCommanders = null;

    private User user;
    public Commande(){

    }
    public Commande(Date date, ArrayList<PlatCommander> platCommanders,int somme){


        this.date = date;
        this.platCommanders = platCommanders;
        this.somme = somme;

    }


    public int getSomme() {
        return somme;
    }

    public void setSomme(int somme) {
        this.somme = somme;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getLivraison() {
        return livraison;
    }

    public void setLivraison(Boolean livraison) {
        this.livraison = livraison;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<PlatCommander> getPlatCommanders() {
        return platCommanders;
    }

    public void setPlatCommanders(ArrayList<PlatCommander> platCommanders) {
        this.platCommanders = platCommanders;
    }

    public boolean isEtat() {
        etat = true;
        for (int i = 0; i <platCommanders.size(); i++){
            if (!platCommanders.get(i).isEtat()) etat = false;
        }
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public boolean isEtat2() {

        for (int i = 0; i <platCommanders.size(); i++){
            if (platCommanders.get(i).isEtat()) etat2 = true;
        }
        return etat2;
    }

    public void setEtat2(boolean etat2) {
        this.etat2 = etat2;
    }
}