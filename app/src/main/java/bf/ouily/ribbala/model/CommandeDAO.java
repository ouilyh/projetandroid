package bf.ouily.ribbala.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;
import bf.ouily.ribbala.outil.Outils;

public class CommandeDAO extends DAO<Commande> {

    private SQLiteDatabase db;
    private UserDAO userDAO;
    private PlatCommaderDAO platCommaderDAO;
    private UserControl userControl = UserControl.getInstance();

    public CommandeDAO(MyDatabaseOpenHelper conn) {
        super(conn);
        userDAO = new UserDAO(conn);

    }

    /**
     * Méthode de création
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean create(Commande obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id",obj.getUser().getId());
        values.put("date",obj.getDate().toString());
        values.put("livraison",obj.getLivraison());

        int id = (int)db.insert("commandes", "", values);
        obj.setId(id);

        return false;
    }
    public int createId(Commande obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id",obj.getUser().getId());
        values.put("date",obj.getDate().toString());
        values.put("livraison",obj.getLivraison());

        int id = (int)db.insert("commandes", "", values);

        return id;
    }

    @Override
    public ArrayList<Commande> select() {
        db = connect.getReadableDatabase();
        ArrayList<Commande> commandes = new ArrayList<Commande>();
        String req = "SELECT * FROM commandes WHERE user_id = "+userControl.getUser().getId();

        Cursor cursor = db.rawQuery(req,null);

        if (cursor.moveToFirst()){
            do {
                Commande commande = new Commande();

                commande.setId(cursor.getInt(0));
                commande.setUser(userDAO.find(cursor.getInt(1)));
                commande.setDate(Outils.StringToDate(cursor.getString(2)));
                commande.setLivraison(Boolean.parseBoolean(cursor.getString(3)));

                commandes.add(commande);

            }while (cursor.moveToNext());
        }

        return commandes;
    }

    /**
     * Méthode pour effacer
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean delete(Commande obj) {
        return false;
    }

    /**
     * Méthode de mise à jour
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean update(Commande obj) {
        return false;
    }

    /**
     * Méthode de recherche des informations
     *
     * @param id
     * @return T
     */
    @Override
    public Commande find(int id) {

        db = connect.getReadableDatabase();

        Commande commande = new Commande();
        String req = "SELECT * FROM commandes WHERE com_id = "+id;

        Cursor cursor = db.rawQuery(req,null);

        cursor.moveToLast();

        if (!cursor.isAfterLast()){

            commande.setId(cursor.getInt(0));
            commande.setUser(userDAO.find(cursor.getInt(1)));
            commande.setDate(Outils.StringToDate(cursor.getString(2)));
            commande.setLivraison(Boolean.parseBoolean(cursor.getString(3)));
        }
        return commande;
    }
}
