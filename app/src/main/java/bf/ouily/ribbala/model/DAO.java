package bf.ouily.ribbala.model;

import java.sql.Connection;
import java.util.ArrayList;

import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public abstract class DAO<T> {

    protected MyDatabaseOpenHelper connect = null;
    public DAO(MyDatabaseOpenHelper conn){
        this.connect = conn;
    }

    /**
     * Méthode de création
     * @param obj
     * @return boolean
     */
    public abstract boolean create(T obj);

    public abstract ArrayList<T> select();
    /**
     * Méthode pour effacer
     * @param obj
     * @return boolean
     */
    public abstract boolean delete(T obj);
    /**
     * Méthode de mise à jour
     * @param obj
     * @return boolean
     */
    public abstract boolean update(T obj);
    /**
     * Méthode de recherche des informations
     * @param id
     * @return T
     */
    public abstract T find(int id);

}