package bf.ouily.ribbala.model;

import java.io.Serializable;
import java.util.Objects;

public class Plat implements Serializable {

    private int id;
    private String nomPlat;
    private String desc;
    private String img;
    private int prix;
    private int type;

    private String nomResto;

    private User user;

    private Restaurant restaurant;

    public Plat(){}

    public Plat(String nomPlat, String desc, String img, int prix, Integer type) {
        this.nomPlat = nomPlat;
        this.desc = desc;
        this.img = img;
        this.prix = prix;
        this.type = type;
    }

    public Plat(String nomPlat, String desc, String img, int prix, Integer type, String nomResto) {
        this.nomPlat = nomPlat;
        this.desc = desc;
        this.img = img;
        this.prix = prix;
        this.type = type;
        this.nomResto = nomResto;
    }

    public String getNomPlat() {
        return nomPlat;
    }

    public void setNomPlat(String nomPlat) {
        this.nomPlat = nomPlat;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plat plat = (Plat) o;
        return  Objects.equals(nomPlat, plat.nomPlat) &&
                Objects.equals(desc, plat.desc)&&
                Objects.equals(type,plat.type);
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nomPlat, desc, img, prix, type);
    }



    //////////////////////////////////////


    public String getNomResto() {
        return nomResto;
    }

    public void setNomResto(String nomResto) {
        this.nomResto = nomResto;
    }
}
