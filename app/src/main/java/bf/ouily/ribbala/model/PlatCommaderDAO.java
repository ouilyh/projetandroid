package bf.ouily.ribbala.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class PlatCommaderDAO extends DAO<PlatCommander> {

    private SQLiteDatabase db;
    private PlatDAO platDAO;
    private CommandeDAO commandeDAO;


    public PlatCommaderDAO(MyDatabaseOpenHelper conn) {
        super(conn);
        platDAO = new PlatDAO(conn);
        commandeDAO = new CommandeDAO(conn);
    }

    /**
     * Méthode de création
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean create(PlatCommander obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("plat_id",obj.getPlat().getId());
        values.put("com_id",obj.getCommande().getId());
        values.put("quantite",obj.getQuantite());
        values.put("etat",obj.isEtat());
        values.put("somme",obj.getSomme());

        db.insert("platcommanders",null,values);
        return false;
    }

    @Override
    public ArrayList<PlatCommander> select() {

        db = connect.getReadableDatabase();
        String req = "SELECT * FROM platcommanders";

        ArrayList<PlatCommander> platCommanders = new ArrayList<PlatCommander>();

        Cursor cursor = db.rawQuery(req,null);

        if (cursor.moveToFirst()) {
            do {
                PlatCommander platCommander = new PlatCommander();
                platCommander.setId(cursor.getInt(0));
                platCommander.setPlat(platDAO.find(cursor.getInt(1)));
                platCommander.setCommande(commandeDAO.find(cursor.getInt(2)));
                platCommander.setQuantite(cursor.getInt(3));
                platCommander.setEtat(Boolean.parseBoolean(cursor.getString(4)));
                platCommander.setSomme(cursor.getInt(5));

                // Ajouter a la liste
                platCommanders.add(platCommander);
            } while (cursor.moveToNext());
        }
        return platCommanders;
    }
    public ArrayList<PlatCommander> selectByCommandeId(Commande commande) {

        db = connect.getReadableDatabase();
        String req = "SELECT * FROM platcommanders WHERE com_id = "+commande.getId();

        ArrayList<PlatCommander> platCommanders = new ArrayList<PlatCommander>();

        Cursor cursor = db.rawQuery(req,null);

        if (cursor.moveToFirst()) {
            do {
                PlatCommander platCommander = new PlatCommander();
                platCommander.setId(cursor.getInt(0));
                platCommander.setPlat(platDAO.find(cursor.getInt(1)));
                platCommander.setCommande(commandeDAO.find(cursor.getInt(2)));
                platCommander.setQuantite(cursor.getInt(3));
                platCommander.setEtat(Boolean.parseBoolean(cursor.getString(4)));
                platCommander.setSomme(cursor.getInt(5));

                // Ajouter a la liste
                platCommanders.add(platCommander);
            } while (cursor.moveToNext());
        }
        return platCommanders;
    }

    public ArrayList<PlatCommander> selectByRestaurantId(Restaurant restaurant) {

        db = connect.getReadableDatabase();
        String req = "" +
                "SELECT platcommanders.platcom_id, platcommanders.plat_id, platcommanders.com_id, platcommanders.quantite,platcommanders.etat, platcommanders.somme" +
                " FROM platcommanders" +
                " INNER JOIN plats " +
                " ON plats.plat_id = platcommanders.plat_id " +
                " AND plats.resto_id = " + restaurant.getId();

        ArrayList<PlatCommander> platCommanders = new ArrayList<PlatCommander>();

        Cursor cursor = db.rawQuery(req,null);

        if (cursor.moveToFirst()) {
            do {
                PlatCommander platCommander = new PlatCommander();
                platCommander.setId(cursor.getInt(0));
                platCommander.setPlat(platDAO.find(cursor.getInt(1)));
                platCommander.setCommande(commandeDAO.find(cursor.getInt(2)));
                platCommander.setQuantite(cursor.getInt(3));
                platCommander.setEtat(Boolean.parseBoolean(cursor.getString(4)));
                platCommander.setSomme(cursor.getInt(5));

                // Ajouter a la liste
                platCommanders.add(platCommander);
            } while (cursor.moveToNext());
        }
        return platCommanders;
    }

    /**
     * Méthode pour effacer
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean delete(PlatCommander obj) {
        return false;
    }

    /**
     * Méthode de mise à jour
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean update(PlatCommander obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("plat_id",obj.getPlat().getId());
        values.put("com_id",obj.getCommande().getId());
        values.put("quantite",obj.getQuantite());
        values.put("etat",obj.isEtat());
        values.put("somme",obj.getSomme());

        db.update("platcommanders",values,"platcom_id = "+obj.getId(),null);
        return false;
    }

    /**
     * Méthode de recherche des informations
     *
     * @param id
     * @return T
     */
    @Override
    public PlatCommander find(int id) {

        db = connect.getReadableDatabase();
        String req = "SELECT * FROM platcommanders WHERE id = "+id;

        PlatCommander platCommander = new PlatCommander();

        Cursor cursor = db.rawQuery(req,null);

        if (!cursor.isAfterLast()) {

            platCommander.setId(cursor.getInt(0));
            platCommander.setPlat(platDAO.find(cursor.getInt(1)));
            platCommander.setCommande(commandeDAO.find(cursor.getInt(2)));
            platCommander.setQuantite(cursor.getInt(3));
            platCommander.setEtat(Boolean.parseBoolean(cursor.getString(4)));
            platCommander.setSomme(cursor.getInt(5));

        }
        return platCommander;
    }
}
