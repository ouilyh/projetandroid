package bf.ouily.ribbala.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class PlatCommander implements Serializable {

    private int id;
    private Plat plat;
    private Commande commande;
    private int quantite;
    private int somme;
    private boolean etat;


    public PlatCommander(){

    }

    public Plat getPlat() {
        return plat;
    }

    public void setPlat(Plat plat) {
        this.plat = plat;
    }


    public synchronized int getQuantite() {
        return quantite;
    }

    public synchronized void setQuantite(int quantite) {

        int oldQuantite = this.quantite;
        this.quantite = quantite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public int getSomme() {
        return somme;
    }

    public void setSomme(int somme) {
        this.somme = somme;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }
}
