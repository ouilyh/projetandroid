package bf.ouily.ribbala.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class PlatDAO extends DAO<Plat> {

    private SQLiteDatabase db;
    private RestaurantDAO restaurantDAO;

    public PlatDAO(MyDatabaseOpenHelper conn) {
        super(conn);
        restaurantDAO = new RestaurantDAO(conn);
    }

    /**
     * Méthode de création
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean create(Plat obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("nomPLat",obj.getNomPlat());
        values.put("description",obj.getDesc());
        values.put("prix",obj.getPrix());
        values.put("typePlat",obj.getType());
        values.put("resto_id",obj.getRestaurant().getId());

        db.insert("plats","",values);
        return false;
    }

    @Override
    public ArrayList<Plat> select() {

        db = connect.getReadableDatabase();
        ArrayList<Plat> plats = new ArrayList<Plat>();
        String selectQuery = "SELECT  * FROM plats";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Plat plat = new Plat();
                plat.setId(Integer.parseInt(cursor.getString(0)));
                plat.setNomPlat(cursor.getString(1));
                plat.setDesc(cursor.getString(2));
                plat.setPrix(cursor.getInt(3));
                plat.setType(cursor.getInt(4));
                plat.setRestaurant(restaurantDAO.find(cursor.getInt(5)));

                // Ajouter a la liste
                plats.add(plat);
            } while (cursor.moveToNext());
        }
        return plats;
    }

    public ArrayList<Plat> selectById(Restaurant restaurant) {

        db = connect.getReadableDatabase();
        ArrayList<Plat> plats = new ArrayList<Plat>();
        String selectQuery = "SELECT  * FROM plats WHERE resto_id = "+restaurant.getId();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Plat plat = new Plat();
                plat.setId(Integer.parseInt(cursor.getString(0)));
                plat.setNomPlat(cursor.getString(1));
                plat.setDesc(cursor.getString(2));
                plat.setPrix(cursor.getInt(3));
                plat.setType(cursor.getInt(4));
                plat.setRestaurant(restaurantDAO.find(cursor.getInt(5)));

                // Ajouter a la liste
                plats.add(plat);
            } while (cursor.moveToNext());
        }
        return plats;
    }
    /**
     * Méthode pour effacer
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean delete(Plat obj) {
        db = connect.getWritableDatabase();
        db.delete("plats","plat_id = "+obj.getId(),null);
        return false;
    }

    /**
     * Méthode de mise à jour
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean update(Plat obj) {

        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("nomPlat",obj.getNomPlat());
        values.put("description",obj.getDesc());
        values.put("prix",obj.getPrix());
        values.put("typePlat",obj.getType());

        db.update("plats",values,"plat_id = "+obj.getId(),null);

        return false;
    }

    /**
     * Méthode de recherche des informations
     *
     * @param id
     * @return T
     */
    @Override
    public Plat find(int id) {
        Plat plat = new Plat();
        db = connect.getReadableDatabase();
        String req = "SELECT * FROM plats WHERE plat_id = "+id;

        Cursor cursor = db.rawQuery(req, null);
        cursor.moveToLast();
        if (!cursor.isAfterLast()){

            plat.setId(Integer.parseInt(cursor.getString(0)));
            plat.setNomPlat(cursor.getString(1));
            plat.setDesc(cursor.getString(2));
            plat.setPrix(cursor.getInt(3));
            plat.setType(cursor.getInt(4));
            plat.setRestaurant(restaurantDAO.find(cursor.getInt(5)));

        }
        return plat;
    }
}
