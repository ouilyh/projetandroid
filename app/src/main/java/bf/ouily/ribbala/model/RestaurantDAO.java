package bf.ouily.ribbala.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class RestaurantDAO extends DAO<Restaurant> {

    private SQLiteDatabase db;
    DAO<User> userDAO;


    public RestaurantDAO(MyDatabaseOpenHelper conn) {
        super(conn);
        userDAO = new UserDAO(conn);
    }

    /**
     * Méthode de création
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean create(Restaurant obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nomRest",obj.getNomRest());
        values.put("adresse",obj.getAdresse());
        values.put("email",obj.getEmail());
        values.put("user_id",obj.getUser().getId());

        db.insert("restaurants","",values);
        return false;
    }

    public int createId(Restaurant obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nomResto",obj.getNomRest());
        values.put("adress",obj.getAdresse());
        values.put("email",obj.getEmail());
        values.put("user_id",obj.getUser().getId());

        long id = db.insert("restaurants","",values);
        return (int)id;

    }

    @Override
    public ArrayList<Restaurant> select() {
        db = connect.getReadableDatabase();

        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();

        String req = "SELECT * FROM restaurants";

        Cursor cursor = db.rawQuery(req, null);
        if (cursor.moveToFirst()){
            do {
                Restaurant restaurant = new Restaurant();
                restaurant.setId(cursor.getInt(0));
                restaurant.setNomRest(cursor.getString(1));
                restaurant.setAdresse(cursor.getString(2));
                restaurant.setEmail(cursor.getString(3));
                restaurant.setUser(userDAO.find(cursor.getInt(4)));

                restaurants.add(restaurant);

            }while (cursor.moveToNext());
        }

        return restaurants;
    }

    /**
     * Méthode pour effacer
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean delete(Restaurant obj) {

        db = connect.getWritableDatabase();

        String req = "DELETE FROM restaurants WHERE resto_id = "+obj.getId();
        String req2 = "DELETE FROM users WHERE user_id = "+obj.getUser().getId();
        db.execSQL(req);
        db.execSQL(req2);

    return false;
    }

    /**
     * Méthode de mise à jour
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean update(Restaurant obj) {
        return false;
    }

    /**
     * Méthode de recherche des informations
     *
     * @param id
     * @return T
     */
    @Override
    public Restaurant find(int id) {
        db = connect.getReadableDatabase();
        Restaurant restaurant = new Restaurant();
        String req = "SELECT * FROM restaurants WHERE resto_id = "+id;

        Cursor cursor = db.rawQuery(req, null);
        cursor.moveToLast();

        if (!cursor.isAfterLast()){

            restaurant.setId(cursor.getInt(0));
            restaurant.setNomRest(cursor.getString(1));
            restaurant.setAdresse(cursor.getString(2));
            restaurant.setEmail(cursor.getString(3));
            restaurant.setUser(userDAO.find(cursor.getInt(4)));
        }

        return restaurant;
    }

    public Restaurant findUserId(int id) {
        db = connect.getReadableDatabase();
        Restaurant restaurant = new Restaurant();
        String req = "SELECT * FROM restaurants WHERE user_id = "+id;

        Cursor cursor = db.rawQuery(req, null);
        cursor.moveToLast();

        if (!cursor.isAfterLast()){

            restaurant.setId(cursor.getInt(0));
            restaurant.setNomRest(cursor.getString(1));
            restaurant.setAdresse(cursor.getString(2));
            restaurant.setEmail(cursor.getString(3));
            restaurant.setUser(userDAO.find(cursor.getInt(4)));
        }

        return restaurant;
    }
}
