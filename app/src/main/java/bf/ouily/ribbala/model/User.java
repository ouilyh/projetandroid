package bf.ouily.ribbala.model;

import java.io.Serializable;

public class User implements Serializable {

    private int id;
    private String nom;
    private String pwd;
    private int typeUser;


    public User(){

    }
    public User(String nom, String pwd){

        this.nom = nom;
        this.pwd = pwd;

    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(int typeUser) {
        this.typeUser = typeUser;
    }
}
