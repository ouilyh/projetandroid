package bf.ouily.ribbala.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.Connection;
import java.util.ArrayList;

import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class UserDAO extends DAO<User> {

    private SQLiteDatabase db;
    public UserDAO(MyDatabaseOpenHelper connection){
        super(connection);
    }
    /**
     * Méthode de création
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean create(User obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("username",obj.getNom());
        values.put("password",obj.getPwd());
        String req = "insert into users (username,password) values";
        req += "('" +obj.getNom()+
                "','" +obj.getPwd()+
                "')";
        long id = db.insert("users","",values);
        Log.d("DB id", "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++create: "+id);
        return false;
    }

    public int createId(User obj) {
        db = connect.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("username",obj.getNom());
        values.put("password",obj.getPwd());
        if (obj.getTypeUser() > 1) values.put("userType", obj.getTypeUser());

        long id = db.insert("users","",values);
        Log.d("DB id", "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++create: "+id);
        return (int)id;
    }

    @Override
    public ArrayList<User> select() {

        db = connect.getReadableDatabase();
        ArrayList<User> users = new ArrayList<User>();
        String selectQuery = "SELECT  * FROM users";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(0)));
                user.setNom(cursor.getString(1));
                user.setPwd(cursor.getString(3));
                user.setTypeUser(Integer.parseInt(cursor.getString(2)));

                // Ajouter a la liste
                users.add(user);
            } while (cursor.moveToNext());
        }

        return users;
    }

    /**
     * Méthode pour effacer
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean delete(User obj) {
        return false;
    }

    /**
     * Méthode de mise à jour
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean update(User obj) {
        return false;
    }

    /**
     * Méthode de recherche des informations
     *
     * @param id
     * @return T
     */
    @Override
    public User find(int id) {

        db = connect.getReadableDatabase();
        User user = new User();
        String selectQuery = "SELECT  * FROM users WHERE user_id = '"+id+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToLast();
        if (!cursor.isAfterLast()) {

            user.setId(cursor.getInt(0));
            user.setNom(cursor.getString(1));
            user.setPwd(cursor.getString(3));
            user.setTypeUser(cursor.getInt(2));

        }
        return user;
    }
    public User findUsername(String username) {

        db = connect.getReadableDatabase();
        User user = new User();
        user.setId(-1);
        String selectQuery = "SELECT  * FROM users WHERE username = '"+username+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToLast();
        if (!cursor.isAfterLast()) {

            user.setId(cursor.getInt(0));
            user.setNom(cursor.getString(1));
            user.setPwd(cursor.getString(3));
            user.setTypeUser(cursor.getInt(2));

        }
        return user;
    }
}
