package bf.ouily.ribbala.outil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDatabaseOpenHelper extends SQLiteOpenHelper {



    //Le noms des tables
    private static final String TABLE_CLIENTS = "clients";
    private static final String TABLE_USERS = "users";
    private static final String TABLE_RESTO = "restaurants";
    private static final String TABLE_PLATS = "plats";
    private static final String TABLE_COMMANDE = "commandes";
    private static final String TABLE_PLATCOMMANDERS = "platcommanders";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "databaseRibbala";


    //Creation des tables
    private String CREATE_CLIENTS_TABLE = "CREATE TABLE "+TABLE_CLIENTS
            +"(" +
            "client_id INTEGER PRIMARY KEY," +
            "nomClient TEXT NOT NULL," +
            "prenomClient TEXT," +
            "adresse TEXT," +
            "numero TEXT," +
            "email TEXT," +
            "user_id INTEGER," +
            "FOREIGN KEY (user_id) REFERENCES " + TABLE_USERS + " (user_id)" +
            "ON DELETE CASCADE"+
            ")";

    private String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS
            +"(" +
            "user_id INTEGER PRIMARY KEY," +
            "username TEXT NOT NULL," +
            "userType INTEGER DEFAULT 1," +
            "password TEXT NOT NULL)";

    private String CREATE_RESTAURANTS_TABLE = "CREATE TABLE "+ TABLE_RESTO +"(" +
            "resto_id INTEGER PRIMARY KEY," +
            "nomResto TEXT NOT NULL," +
            "adress TEXT," +
            "email TEXTE," +
            "user_id INTEGER NOT NULL," +
            "FOREIGN KEY (user_id) REFERENCES "+TABLE_USERS+" (user_id)"+
            ")";

    private String CREATE_PLATS_TABLE = "CREATE TABLE "+TABLE_PLATS +"("
            +"plat_id INTEGER PRIMARY KEY," +
            "nomPlat TEXT NOT NULL," +
            "description TEXT," +
            "prix INTEGER NOT NULL," +
            "typePlat INTERGER NOT NULL," +
            "resto_id INTEGER NOT NULL," +
            "FOREIGN KEY (resto_id) REFERENCES "+TABLE_RESTO+" (user_id)"+
            ")";

    private String CREATE_PLATCOMMANDERS_TABLE = "CREATE TABLE " + TABLE_PLATCOMMANDERS + "(" +
            "platcom_id INTEGER PRIMARY KEY," +
            "plat_id INTERGER," +
            "com_id INTEGER," +
            "quantite INTEGER NOT NULL," +
            "etat TEXTE," +
            "somme INTEGER,"+
            "FOREIGN KEY (plat_id) REFERENCES "+TABLE_PLATS+"(plat_id)," +
            "FOREIGN KEY (com_id) REFERENCES "+TABLE_COMMANDE+"(com_id)" +
            ")";

    private String CREATE_COMMANDES_TABLE = "CREATE TABLE "+ TABLE_COMMANDE +"(" +
            "com_id INTEGER PRIMARY KEY," +
            "user_id INTEGER," +
            "date TEXT," +
            "somme INTEGER,"+
            "etat TEXT,"+
            "livraison TEXT," +
            "FOREIGN KEY (user_id) REFERENCES "+TABLE_CLIENTS+"(user_id)" +
            ")";


    private String insertAdmin = "INSERT INTO users (username,userType,password) values" +
            "('admin',3,'admin')";
    public MyDatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_CLIENTS_TABLE);
        db.execSQL(CREATE_RESTAURANTS_TABLE);
        db.execSQL(CREATE_PLATS_TABLE);
        db.execSQL(CREATE_COMMANDES_TABLE);
        db.execSQL(CREATE_PLATCOMMANDERS_TABLE);

        db.execSQL(insertAdmin);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLATS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMANDE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLATCOMMANDERS);

        // Create tables again
        onCreate(db);


    }
}
