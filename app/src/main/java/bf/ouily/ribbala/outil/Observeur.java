package bf.ouily.ribbala.outil;

    public interface Observeur {

        void onChange(boolean change); // params need to change as per your use case.
}
