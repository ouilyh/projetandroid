package bf.ouily.ribbala.outil;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bf.ouily.ribbala.model.PlatCommander;

public class Outils {

    public static int PlatCommandeSize(ArrayList<PlatCommander> platCommanders){
        int nmb = 0;
        if (platCommanders == null) nmb = 0;
        else {
            for (int i = 0; i< platCommanders.size(); i++){
                nmb += platCommanders.get(i).getQuantite();
            }
        }
        return nmb;
    }

    public static Date StringToDate(String date){
        String patern = "EEE MMM dd hh:mm:ss 'GMT' yyyy";

        Date date1 = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat(patern);

        try {
            date1 = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date1;

    }

    public static String typePlate(int type) {
        if (type == 1)
            return "Boisson";
        if (type == 2)
            return "Desert";

        return null;
    }
}
