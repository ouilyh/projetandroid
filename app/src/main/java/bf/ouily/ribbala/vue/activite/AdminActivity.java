package bf.ouily.ribbala.vue.activite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Restaurant;
import bf.ouily.ribbala.vue.adpter.AdminListeAdapter;

public class AdminActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    private ListView adminL;


    private ArrayList<Restaurant> listRestaurant = new ArrayList<Restaurant>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        //listRestaurant = new AccessLocal(getApplicationContext()).getRestaurantDAO().select();

        listRestaurant = RestaurantControl.getInstance(getApplicationContext()).getList();
        Log.d("Resto************************", "+++++++++++++++++++++++++onCreate: "+listRestaurant.size());
        init();

        AdminListeAdapter adminListeAdapter = new AdminListeAdapter(getApplicationContext(),listRestaurant);
        adminL.setAdapter(adminListeAdapter);


    }

    private void init(){
        
        // Toolbar
        toolbar = (androidx.appcompat.widget.Toolbar)findViewById(R.id.plat_toolbar);
        toolbar.setTitle("Liste des restaurants");
        drawerLayout = (DrawerLayout) findViewById(R.id.admin_drawer);
        navigationView = (NavigationView) findViewById(R.id.plat_navigation_view);

        this.configueToolbar();

        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.drawer_menu_restaurant).setVisible(false);


        //List view

        adminL = (ListView) findViewById(R.id.admin_lv);



    }

    public void configueToolbar(){

        setSupportActionBar(toolbar);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        Intent i;
        switch(item.getItemId()) {/*...*/

            case R.id.drawer_ajout_partenaire:
                i = new Intent(AdminActivity.this,AjouterRestaurantActivity2.class);
                startActivity(i);
                break;

        }
        drawerLayout.closeDrawer(GravityCompat.START);

        item.setChecked(true);
        // Set action bar title
        setTitle(item.getTitle());
        // Close the navigation drawer
        drawerLayout.closeDrawers();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

        AdminListeAdapter adminListeAdapter = new AdminListeAdapter(getApplicationContext(),listRestaurant);
        adminL.setAdapter(adminListeAdapter);

    }

}