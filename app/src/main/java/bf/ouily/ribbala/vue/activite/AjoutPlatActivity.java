package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.model.Plat;

public class AjoutPlatActivity extends AppCompatActivity {

    private TextView nomPlat;
    private TextView desc;
    private TextView prix;
    private Spinner spinner;
    private int typePlat;
    private Button img;
    private Button valider;

    private PlatControl platControl = PlatControl.getInstance();


    int SELECT_PICTURE = 200;

    String[] typeplat = {"Boisson","Desert","Bergeur"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_plat);

        init();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AjoutPlatActivity.this, "Valider", Toast.LENGTH_SHORT).show();
                valider();
            }
        });

    }

    private void init(){

        nomPlat = findViewById(R.id.ajout_plat_nom);
        desc = findViewById(R.id.ajout_plat_desc);
        prix = findViewById(R.id.ajout_plat_prix);
        spinner = findViewById(R.id.ajout_plat_spinner);
        img = findViewById(R.id.ajout_plat_img_btn);
        valider = findViewById(R.id.ajout_plat_valider);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,typeplat);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( typeplat[position]=="Desert" ) typePlat = 2;
                if ( typeplat[position]=="Boisson" ) typePlat = 1;
                if ( typeplat[position]=="Burgeur" ) typePlat = 3;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void valider(){

        if (!nomPlat.getText().toString().matches("") &&
                !desc.getText().toString().matches("") &&
                !prix.getText().toString().matches("") ){

            Plat plat = new Plat();
            plat.setNomPlat(nomPlat.getText().toString());
            plat.setDesc(desc.getText().toString());
            plat.setPrix(Integer.parseInt(prix.getText().toString()));

            plat.setType(typePlat);
            plat.setRestaurant(RestaurantControl.getInstance(getApplicationContext()).getRestaurant());
            platControl.addPlat(plat,getApplicationContext());

            onBackPressed();

        }else Toast.makeText(getApplicationContext(), "Veuillez rendeigner tous les champs", Toast.LENGTH_SHORT).show();


    }

    private void chargeImage(){

        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);


        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            // compare the resultCode with the
            // SELECT_PICTURE constant
            if (requestCode == SELECT_PICTURE) {
                // Get the url of the image from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // update the preview image in the layout
                    Toast.makeText(this, "img" + selectedImageUri.getPath(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}