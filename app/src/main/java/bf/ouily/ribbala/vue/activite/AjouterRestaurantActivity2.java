package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Restaurant;
import bf.ouily.ribbala.model.RestaurantDAO;
import bf.ouily.ribbala.model.User;

public class AjouterRestaurantActivity2 extends AppCompatActivity {

    private TextInputEditText nomResto;
    private TextInputEditText adresse;
    private TextInputEditText mail;
    private TextInputEditText pseudo;
    private TextInputEditText pass;
    private Button valideBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_restaurant2);
        init();
    }

    private void init(){

        nomResto = findViewById(R.id.ajout_part_nom);
        adresse = findViewById(R.id.ajout_part_adresse);
        mail = findViewById(R.id.ajout_part_mail);
        pseudo = findViewById(R.id.ajout_part_username);
        pass = findViewById(R.id.ajout_part_pass);
        valideBtn = findViewById(R.id.ajout_part_valide_btn);

        valideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enregistrer();
            }
        });

    }

    private void enregistrer(){
        if (!nomResto.getText().toString().matches("") && !adresse.getText().toString().matches("") && !mail.getText().toString().matches("") &&
                !pseudo.getText().toString().matches("") && !valideBtn.getText().toString().matches("")){
            AccessLocal accessLocal = new AccessLocal(getApplicationContext());
            User user = new User();
            user.setNom(pseudo.getText().toString());
            user.setPwd(pass.getText().toString());
            user.setTypeUser(2);
            int id = accessLocal.getUserDAO().createId(user);
            if (id > -1) {
                Restaurant restaurant = new Restaurant();
                user.setId(id);
                restaurant.setUser(user);
                restaurant.setEmail(mail.getText().toString());
                restaurant.setAdresse(adresse.getText().toString());
                restaurant.setNomRest(nomResto.getText().toString());

                int id2 = accessLocal.getRestaurantDAO().createId(restaurant);

                RestaurantControl.getInstance(getApplicationContext()).addRestaurant(accessLocal.getRestaurantDAO().find(id2));

                onBackPressed();
            }
        }else Toast.makeText(this, "Erreur", Toast.LENGTH_SHORT).show();
    }
}