package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.CommandeControl;
import bf.ouily.ribbala.model.Commande;
import bf.ouily.ribbala.vue.adpter.CommandeAdapter;

public class CommandeActivity extends AppCompatActivity {
    private ListView listView;
    private CommandeControl commandeControl = CommandeControl.getInstance();
    private ArrayList<Commande> list = null;

    private Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commande);
        init();
    }

    private void init(){
        //Toolbar
        toolbar = (Toolbar)findViewById(R.id.commande_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Liste des commandes");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        listView = findViewById(R.id.commande_lv);
        commandeControl.setContext(CommandeActivity.this);
        list = commandeControl.getCommandes();
        if (list == null) list = new ArrayList<Commande>();

        CommandeAdapter commandeAdapter = new CommandeAdapter(getApplicationContext(),list);
        listView.setAdapter(commandeAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(CommandeActivity.this,CommandeDetailActivity.class);
                i.putExtra("commande",commandeControl.getCommandes().get(position));
                startActivity(i);
            }
        });
    }
}