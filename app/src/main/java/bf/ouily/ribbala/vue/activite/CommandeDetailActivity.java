package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.CommandeControl;
import bf.ouily.ribbala.model.Commande;
import bf.ouily.ribbala.model.Plat;

public class CommandeDetailActivity extends AppCompatActivity {

    private Commande commande;

    private ListView detailCommande;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commande_detail);
        init();

        Intent i = getIntent();
        commande = (Commande) i.getSerializableExtra("commande");
        CommandeDetail commandeDetail = new CommandeDetail(getApplicationContext(),commande.getPlatCommanders());
        detailCommande.setAdapter(commandeDetail);

    }

    private void init(){

        toolbar = (Toolbar)findViewById(R.id.detail_commande_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail de la commande");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        detailCommande = findViewById(R.id.commande_detail_lv);

    }
}