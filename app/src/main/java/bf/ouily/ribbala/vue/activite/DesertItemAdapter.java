package bf.ouily.ribbala.vue.activite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.model.Plat;

public class DesertItemAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Plat> data;
    private int type = 0;
    private Boolean resto = false;

    private Context context;

    public DesertItemAdapter(Context context, ArrayList<Plat> data){
        this.inflater = LayoutInflater.from(context);
        this.data = PlatControl.getInstance().getListPlat();
    }
    public DesertItemAdapter(Context context, ArrayList<Plat> data, int type, Boolean resto){
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.type = type;
        this.resto = resto;
        this.context = context;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if ( convertView == null){

            convertView = inflater.inflate(R.layout.plat_item,null);

            holder = new Holder();
            holder.item_name = (TextView) convertView.findViewById(R.id.item_plat_name);
            holder.item_type = (TextView) convertView.findViewById(R.id.item_plat_type);
            holder.item_prix = (TextView) convertView.findViewById(R.id.item_plat_prix);

            convertView.setTag(holder);
        }else {
            holder = (Holder)convertView.getTag();
        }

        if (resto){

            if (data.get(position).getType() == type && RestaurantControl.getInstance(context).getUser().getId() == data.get(position).getUser().getId()){

                holder.item_name.setText(data.get(position).getNomPlat());
                holder.item_prix.setText(""+data.get(position).getPrix());

                if(data.get(position).getType() == 1)
                    holder.item_type.setText("Boisson");

                if(data.get(position).getType() == 2)
                    holder.item_type.setText("Desert");
            }

        }else {

            if (data.get(position).getType() == type ){

                holder.item_name.setText(data.get(position).getNomPlat());
                holder.item_prix.setText(""+data.get(position).getPrix());

                if(type == 1)
                    holder.item_type.setText("Boisson");

                if(type == 2)
                    holder.item_type.setText("Desert");
            }

        }

        return convertView;
    }

    private class  Holder{
        TextView item_name;
        TextView item_type;
        TextView item_prix;
    }
}
