package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatCommanderControl;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.model.PlatCommander;

public class DetailActivity extends AppCompatActivity {

    private int position = 0;

    //Toolbar
    private Toolbar toolbar;
    private TextView item_name;
    private TextView item_desc;
    private TextView item_prix;

    private PlatCommanderControl platCommanderControl = PlatCommanderControl.getInstance();

    private Plat plat;
    private PlatCommander platCommander = null;

    // boutton

    private LinearLayout linearLayout;
    private Button btn;
    private Button btn1;
    private Button btn2;
    private Button btnOk;
    private TextView tv;
    private int quantite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        init();

        Intent i = getIntent();
        plat = (Plat) i.getSerializableExtra("plat");

        ArrayList<PlatCommander> platCommanders = platCommanderControl.getPlatCommanders();
        if (platCommanders != null){

            for (int j=0; j<platCommanders.size(); j++){
                // Lorsque le plat commmander est deja dans le panier
                if (platCommanders.get(j).getPlat().equals(plat)){
                    platCommander = platCommanders.get(j);
                    position = j;
                }
            }
        }

        if(platCommander != null){
            platCommander.setPlat(platCommander.getPlat() );
            platCommander.setQuantite(platCommander.getQuantite());
            item_name.setText(platCommander.getPlat().getNomPlat());
            item_desc.setText(platCommander.getPlat().getDesc());
            item_prix.setText(""+platCommander.getPlat().getPrix());
            quantite = platCommander.getQuantite();
            tv.setText(""+quantite);

        }
        else{
            item_name.setText(plat.getNomPlat());
            item_desc.setText(plat.getDesc());
            item_prix.setText(""+plat.getPrix());
            quantite = Integer.parseInt(tv.getText().toString());
        }

        btnStatus();
        ecouteBouton(this);
        ecouteBouton1(this);
        ecouteBouton2(this);

    }

    private void init(){

        //Toolbar
        toolbar = (Toolbar)findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail du plat");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        item_name = (TextView) findViewById(R.id.detail_plat_name);
        item_desc = (TextView) findViewById(R.id.detail_plat_desc);
        item_prix = (TextView) findViewById(R.id.detail_plat_prix);


        linearLayout = (LinearLayout) findViewById(R.id.quantite_btns);
        btn = (Button) findViewById(R.id.commander);
        btn1 = (Button) findViewById(R.id.btn_moins);
        btn2 = (Button) findViewById(R.id.btn_plus);
        btnOk = (Button) findViewById(R.id.btn_ok);
        tv = (TextView) findViewById(R.id.quantite_tv);



        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commanderPlat();
            }
        });
    }

    private void ecouteBouton(Context context){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
            }
        });

    }

    private void ecouteBouton1(Context context){
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantite--;
                tv.setText(""+quantite);
                btnStatus();
            }
        });

    }

    private void ecouteBouton2(Context context){
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantite++;
                tv.setText(""+quantite);
                btnStatus();
            }
        });

    }

    private void btnStatus(){
        if (quantite <= 0 ){
            btn1.setVisibility(View.GONE);
            btnOk.setVisibility(View.GONE);

        }else{
            btn1.setVisibility(View.VISIBLE);
            btnOk.setVisibility(View.VISIBLE);

        }
    }

    private void commanderPlat(){
        int somme = 0;

        if (platCommander != null){
            int presSomme = platCommanderControl.getPlatCommanders().get(position).getQuantite()*plat.getPrix();
            platCommanderControl.getPlatCommanders().get(position).setQuantite(quantite);

            somme = platCommanderControl.getSomme()-presSomme+quantite*plat.getPrix();

        }else{

            platCommander = new PlatCommander();
            platCommander.setPlat(plat);
            platCommander.setQuantite(quantite);
            platCommanderControl.addPlatCommander(platCommander);

            somme = platCommanderControl.getSomme()+quantite*plat.getPrix();
        }

        platCommanderControl.setSomme(somme);

        Toast.makeText(getApplicationContext(),"commande ajouter",Toast.LENGTH_LONG).show();
        onBackPressed();

    }


}