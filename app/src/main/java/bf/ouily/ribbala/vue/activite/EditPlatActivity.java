package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.model.Plat;

public class EditPlatActivity extends AppCompatActivity {

    private Plat plat = null;
    private TextView nomPlat;
    private TextView desc;
    private TextView prix;
    private int typePlat;
    private Button valideButton;

    private Spinner sTypePlat;

    private PlatControl platControl = PlatControl.getInstance();

    String[] typeplat = {"Boisson","Desert","Bergeur"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_plat);

        init();
        recupePlat();

        valideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valider();
                onBackPressed();
            }
        });

    }

    private void init(){
        nomPlat = findViewById(R.id.edit_plat_nom);
        desc = findViewById(R.id.edit_plat_desc);
        prix = findViewById(R.id.edit_plat_prix);

        sTypePlat = findViewById(R.id.edit_plat_spinner);

        valideButton = findViewById(R.id.edit_plat_valider);

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,typeplat);
        sTypePlat.setAdapter(stringArrayAdapter);

        sTypePlat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( typeplat[position]=="Desert" ) typePlat = 2;
                if ( typeplat[position]=="Boisson" ) typePlat = 1;
                if ( typeplat[position]=="Burgeur" ) typePlat = 3;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        Intent i = getIntent();
        plat = (Plat) i.getSerializableExtra("plat");
    }

    private void recupePlat(){

        if (plat != null){
            nomPlat.setText(plat.getNomPlat());
            desc.setText(plat.getDesc());
            prix.setText(""+plat.getPrix());

            if ( plat.getType()==2 ) sTypePlat.setSelection(1);
            if ( plat.getType()==1 ) sTypePlat.setSelection(0);
            if ( plat.getType()==3 ) sTypePlat.setSelection(2);

        }
    }

    private void valider(){

        plat.setNomPlat(nomPlat.getText().toString());
        plat.setDesc(desc.getText().toString());
        plat.setPrix(Integer.parseInt(prix.getText().toString()));

        plat.setType(typePlat);

        platControl.updatePlat(plat,getApplicationContext());

    }
}