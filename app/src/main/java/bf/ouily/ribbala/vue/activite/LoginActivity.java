package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.ClientControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Client;
import bf.ouily.ribbala.model.ClientDAO;
import bf.ouily.ribbala.model.User;
import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;

public class LoginActivity extends AppCompatActivity {

    private EditText username;
    private  EditText pwd;
    private Button login;




    private AccessLocal local;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
         local = new AccessLocal(this);

        init();

        TextView textView = (TextView) findViewById(R.id.login_register_tv);
        Button login = (Button) findViewById(R.id.login_login_btn);
        ecouteBoutton(textView,RegisterActivity.class);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seConnecter();
            }
        });
    }

    private void init(){
        username = findViewById(R.id.login_username);
        pwd = findViewById(R.id.login_user_pss);

    }

    private void seConnecter(){


        if (!username.getText().toString().matches("") && !pwd.getText().toString().matches("")){
            User user = new User();
            user = local.getUserDAO().findUsername(username.getText().toString());

            if (user.getId() > -1){
                Log.d("Mote de passe", "seConnecter: "+user.getPwd().toString());
                Log.d("Mote de passe", "seConnecter: "+pwd.getText());

                if (pwd.getText().toString().equals(user.getPwd())){
                    Intent intent;


                    if (user.getTypeUser() == 1){
                        ClientControl.getInstance().setClient(local.getClientDAO().findUserId(user.getId()));
                        intent = new Intent(LoginActivity.this, PlatActivity2.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }else if (user.getTypeUser() == 2){
                        RestaurantControl.getInstance(getApplicationContext()).setRestaurant(local.getRestaurantDAO().findUserId(user.getId()));
                        intent =new Intent(LoginActivity.this, RestautrantActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }else {
                        intent = new Intent(LoginActivity.this, AdminActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                }else Toast.makeText(this, "Mot de passe incorrect", Toast.LENGTH_SHORT).show();
            }else Toast.makeText(this, "Nom d'utilisateur incorrect", Toast.LENGTH_SHORT).show();

            UserControl.getInstance().setUser(user);
        } else Toast.makeText(this, "Veuillez renseigner tous les champs", Toast.LENGTH_SHORT).show();



    }

    private void ecouteBoutton(View btn, Class classe){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),classe);
                startActivity(i);
            }
        });
    }
}