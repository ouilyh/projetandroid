package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import bf.ouily.ribbala.R;

public class MainActivity extends AppCompatActivity {

    private Button login;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = (Button)findViewById(R.id.home_login_btn);
        register = (Button)findViewById(R.id.home_register_btn);
        ecouteBoutton(login,LoginActivity.class);
        ecouteBoutton(register,RegisterActivity.class);
    }

    private void ecouteBoutton(Button btn, Class classe){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),classe);
                startActivity(i);
            }
        });
    }
}