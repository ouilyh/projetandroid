package bf.ouily.ribbala.vue.activite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;


import com.google.android.material.navigation.NavigationView;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatCommanderControl;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.model.Commande;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.model.Restaurant;
import bf.ouily.ribbala.model.User;
import bf.ouily.ribbala.outil.MyDatabaseOpenHelper;
import bf.ouily.ribbala.vue.fragment.BoissonFragment;
import bf.ouily.ribbala.vue.fragment.DesertFragment;
import bf.ouily.ribbala.vue.fragment.PanierFragment;

public class PlatActivity2 extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //controlleur
    private PlatControl platControl = PlatControl.getInstance();
    private static PlatCommanderControl platCommanderControl = PlatCommanderControl.getInstance();

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private static ChipNavigationBar navigationBar;

    private boolean admin = false;

    //Drawer navigation

    private NavigationView navigationView;


    // Proprietes
    private ArrayList<Plat> list = new ArrayList<Plat>();

    private ArrayList<Plat> boison = null;
    private ArrayList<Plat> desert = null;

    private int type;
    private  User user;
    private UserControl userControl = UserControl.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plat);

        type = 2;
        init();

        //******************************************
        categorisation();
        //******************************************

        navigationBar.setItemSelected(R.id.menu_type_plat_boison,true);
        BoissonFragment b = new BoissonFragment();
        b.setArrayList(boison);
        getSupportFragmentManager().beginTransaction().replace(R.id.plat_fragment_content, b).commit();

        bottomMenu();

    }

    private void init(){

        type = userControl.getUser().getTypeUser();

        // Toolbar
        toolbar = (androidx.appcompat.widget.Toolbar)findViewById(R.id.plat_toolbar);
        navigationBar = (ChipNavigationBar) findViewById(R.id.plat_navigation_bar);
        drawerLayout = (DrawerLayout) findViewById(R.id.plat_drawer);

        //Recuperation de la liste des plat
        platControl.setContext(getApplicationContext());
        list = platControl.getListPlat();

        //setupDrawerContent();
        navigationView = (NavigationView) findViewById(R.id.plat_navigation_view);



        Menu menu = navigationView.getMenu();
        if (type == 1){
            menu.findItem(R.id.drawer_menu_administrateur).setVisible(false);
            menu.findItem(R.id.drawer_menu_restaurant).setVisible(false);
        }else if (type == 2){
            menu.findItem(R.id.drawer_menu_administrateur).setVisible(false);
        }else menu.findItem(R.id.drawer_menu_restaurant).setVisible(false);


        navigationView.setNavigationItemSelectedListener(this);

        //Toolbar
        this.configueToolbar();

    }

    public void configueToolbar(){

        setSupportActionBar(toolbar);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.drawer_menu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    protected void onResume() {
        super.onResume();
        categorisation();
        updateBadgeValue();
    }

    // mettre les information du badge a jour

    public static void updateBadgeValue(){

        navigationBar.showBadge(R.id.menu_panier,PlatCommanderControl.getInstance().getPlatCommanders().size());
    }

    private void bottomMenu() {

        navigationBar.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int i) {
                Fragment fragment = null;
                switch (i) {
                    case R.id.menu_type_plat_boison:

                        fragment =(BoissonFragment) new BoissonFragment();
                        ((BoissonFragment) fragment).setArrayList(boison);
                        break;
                    case R.id.menu_type_plat_desert:
                        fragment = new DesertFragment();
                        ((DesertFragment) fragment).setList(desert);
                        break;
                    case R.id.menu_panier:
                        fragment = new PanierFragment();
                        break;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.plat_fragment_content,fragment).commit();
            }
        });

    }

    private void categorisation(){
        boison = new ArrayList<Plat>();
        desert = new ArrayList<Plat>();

        for( int j = 0; j < list.size(); j++){

            if (list.get(j).getType()== 1){
                boison.add(list.get(j));
            }
            if (list.get(j).getType()== 2) {
                desert.add(list.get(j));
            }

        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        Intent i;
        switch(item.getItemId()) {/*...*/
            case R.id.drawer_home:
                i = new Intent(PlatActivity2.this, PlatActivity2.class);
                startActivity(i);
                break;
            case R.id.drawer_client_commande:
                i = new Intent(PlatActivity2.this, CommandeActivity.class);
                startActivity(i);
                Toast.makeText(getApplicationContext(),"Menu commade",Toast.LENGTH_LONG).show();
                break;
            case R.id.drawer_ajouter_plat:
                i = new Intent(PlatActivity2.this, AjoutPlatActivity.class);
                startActivity(i);
                break;
            case R.id.drawer_list_plat:
                i = new Intent(PlatActivity2.this, RestautrantActivity.class);
                startActivity(i);
                break;
            case R.id.drawer_ajout_partenaire:
                i = new Intent(PlatActivity2.this,AjouterRestaurantActivity2.class);
                startActivity(i);
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);

        item.setChecked(true);
        // Set action bar title
        setTitle(item.getTitle());
        // Close the navigation drawer
        drawerLayout.closeDrawers();
        return true;
    }
}

