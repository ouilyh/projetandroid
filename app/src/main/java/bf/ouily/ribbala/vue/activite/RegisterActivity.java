package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.ClientControl;
import bf.ouily.ribbala.controlleur.PlatCommanderControl;
import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Client;
import bf.ouily.ribbala.model.Commande;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.model.User;
import bf.ouily.ribbala.model.UserDAO;

public class RegisterActivity extends AppCompatActivity {

    private  TextView nomTv;
    private  TextView prenomTv;
    private  TextView adresseTv;
    private  TextView numeroTv;
    private  TextView emailTv;
    private Button registerBtn;
    private TextView loginTv;

    private Dialog userDialog;

    private int idInsert;

    private AccessLocal accessLocal;

    private ClientControl clientControl = ClientControl.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        TextView textView = (TextView) findViewById(R.id.register_login_tv);
        Button login = (Button) findViewById(R.id.register_login_btn);
        ecouteBoutton(textView,LoginActivity.class);

        accessLocal = new AccessLocal(getApplicationContext());
        init();

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inscriptionClient();

            }
        });

    }


    private void init(){

        nomTv = findViewById(R.id.register_nom_tv);
        prenomTv = findViewById(R.id.register_prenom_tv);
        adresseTv = findViewById(R.id.register_adresse_tv);
        numeroTv = findViewById(R.id.register_num_tv);
        emailTv = findViewById(R.id.register_email_tv);
        registerBtn = findViewById(R.id.register_login_btn);
        loginTv = findViewById(R.id.login_register_tv);
    }

    private void ecouteBoutton(View btn, Class classe){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),classe);
                startActivity(i);
            }
        });
    }

    //Inscription

    private void inscriptionClient(){

        if (!nomTv.getText().toString().matches("") && !prenomTv.getText().toString().matches("") && !adresseTv.getText().toString().matches("") && !numeroTv.getText().toString().matches("") && !emailTv.getText().toString().matches("") ){
            Client client= new Client();
            client.setNomClient(nomTv.getText().toString());
            client.setPrenomClient(prenomTv.getText().toString());
            client.setAdresse(adresseTv.getText().toString());
            client.setNumTel(numeroTv.getText().toString());
            client.setEmail(emailTv.getText().toString());

            dialogUserRegister(client);

            Toast.makeText(this, "Registerment reussi", Toast.LENGTH_SHORT).show();


        }else {

            Toast.makeText(this, "Veillez remplir tout les champs", Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogUserRegister( Client client){
        idInsert = -1;
        userDialog = new Dialog(RegisterActivity.this);
        userDialog.setContentView(R.layout.custm_register_dialog);
        userDialog.getWindow().getAttributes().windowAnimations = R.style.animation;
        userDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        userDialog.setCancelable(false);

        Button annuler = userDialog.findViewById(R.id.user_dialog_annul_btn);
        Button commander = userDialog.findViewById(R.id.user_dialog_valide_btn);

        TextView username = userDialog.findViewById(R.id.dialog_userame);
        TextView pwd = userDialog.findViewById(R.id.dialog_user_pss);
        TextView pwd1 = userDialog.findViewById(R.id.dialog_user_pss_conf);

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idInsert = -1;
                userDialog.dismiss();
            }
        });

        commander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!username.getText().toString().matches("") && !pwd.getText().toString().matches("") && !pwd1.getText().toString().matches("")){

                    if (pwd.getText().toString().equals(pwd1.getText().toString())){

                        User user = new User();
                        user.setNom(username.getText().toString());
                        user.setPwd(pwd.getText().toString());
                        idInsert = accessLocal.getUserDAO().createId(user);

                        client.setUser(accessLocal.getUserDAO().find(idInsert));
                        accessLocal.getClientDAO().create(client);
                        clientControl.setClient(client);

                        Intent intent = new Intent(RegisterActivity.this, PlatActivity2.class);
                        startActivity(intent);
                        userDialog.dismiss();
                    }
                    else{
                        Toast.makeText(RegisterActivity.this, "Les mots de passe de correspondent pas", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
        userDialog.show();
    }
}