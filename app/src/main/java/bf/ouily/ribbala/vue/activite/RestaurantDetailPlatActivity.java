package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.outil.Outils;

public class RestaurantDetailPlatActivity extends AppCompatActivity {

    private int position = 0;

    //Toolbar
    private Toolbar toolbar;
    private TextView item_name;
    private TextView item_desc;
    private TextView item_prix;
    private TextView item_type;
    private TextView item_resto;

    private Button edit;
    private Button delete;


    private Plat plat = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_detail_plat);

        init();
    }

    private void init() {
        Intent i = getIntent();
        plat = (Plat) i.getSerializableExtra("plat");

        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail du plat");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        item_name = (TextView) findViewById(R.id.resto_plat_detail_nom);
        item_desc = (TextView) findViewById(R.id.resto_plat_detail_desc);
        item_prix = (TextView) findViewById(R.id.resto_plat_detail_prix);
        item_type = (TextView) findViewById(R.id.resto_plat_detail_type);
        item_resto = (TextView) findViewById(R.id.resto_plat_detail_nom_resto);

        edit = findViewById( R.id.resto_plat_detail_edit);
        delete = findViewById( R.id.resto_plat_detail_delete);

        if (plat != null) {
            item_name.setText(plat.getNomPlat());
            item_desc.setText(plat.getDesc());
            item_prix.setText("" + plat.getPrix());
            item_type.setText(Outils.typePlate(plat.getType()));
            item_resto.setText(plat.getRestaurant().getNomRest());
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), EditPlatActivity.class);
                i.putExtra("plat",plat);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                startActivity(i);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlatControl.getInstance().delPlat(plat,getApplicationContext());
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}