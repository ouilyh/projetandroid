package bf.ouily.ribbala.vue.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatCommanderControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.PlatCommander;
import bf.ouily.ribbala.vue.adpter.PanierPlatItemAdapter;

public class RestaurantPlatCommdeActivity extends AppCompatActivity {

    private ListView listPlatCom;
    private Toolbar toolbar;
    private ArrayList<PlatCommander> platCommanders =  new ArrayList<PlatCommander>();
    private PlatCommanderControl platCommanderControl = PlatCommanderControl.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_plat_commde);
        listPlatCom = findViewById(R.id.resto_plat_com_lv);
    }

    @Override
    protected void onResume() {
        super.onResume();

        platCommanders = new AccessLocal(getApplicationContext()).getPlatCommaderDAO().selectByRestaurantId(RestaurantControl.getInstance().getRestaurant());
        PanierPlatItemAdapter panierPlatItemAdapter = new PanierPlatItemAdapter(getApplicationContext(),platCommanders);
        listPlatCom.setAdapter(panierPlatItemAdapter);
    }
}