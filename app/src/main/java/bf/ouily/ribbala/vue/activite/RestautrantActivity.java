package bf.ouily.ribbala.vue.activite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.vue.adpter.ListePlatItemAdapter;

public class RestautrantActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    private ListView resto_lv;
    private ArrayList<Plat> list = new ArrayList<Plat>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restautrant);
        init();
    }

    private void init(){

        // Toolbar
        toolbar = (androidx.appcompat.widget.Toolbar)findViewById(R.id.resto_toolbar);
        toolbar.setTitle("Liste des plats");
        drawerLayout = (DrawerLayout) findViewById(R.id.resto_drawer);
        navigationView = (NavigationView) findViewById(R.id.resto_navigation_view);

        resto_lv = findViewById(R.id.resto_lv);

        this.configueToolbar();

        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.drawer_menu_administrateur).setVisible(false);

    }

    public void configueToolbar(){

        setSupportActionBar(toolbar);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        Intent i;
        switch(item.getItemId()) {/*...*/

            case R.id.drawer_home:
                i = new Intent(RestautrantActivity.this, PlatActivity2.class);
                startActivity(i);
                break;
            case R.id.drawer_client_commande:
                i = new Intent(RestautrantActivity.this, CommandeActivity.class);
                startActivity(i);
                Toast.makeText(getApplicationContext(),"Menu commade",Toast.LENGTH_LONG).show();
                break;
            case R.id.drawer_ajouter_plat:
                i = new Intent(RestautrantActivity.this, AjoutPlatActivity.class);
                startActivity(i);
                break;
            case R.id.drawer_list_plat:
                i = new Intent(RestautrantActivity.this, RestautrantActivity.class);
                startActivity(i);
                break;
            case R.id.drawer_list_commamde:
                i = new Intent(RestautrantActivity.this, RestaurantPlatCommdeActivity.class);
                startActivity(i);
                break;

        }
        drawerLayout.closeDrawer(GravityCompat.START);

        item.setChecked(true);
        // Set action bar title
        setTitle(item.getTitle());
        // Close the navigation drawer
        drawerLayout.closeDrawers();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        PlatControl.getInstance().setContext(getApplicationContext());
        list = PlatControl.getInstance().getListPlatRestaurant();
        ListePlatItemAdapter listePlatItemAdapter = new ListePlatItemAdapter(getApplicationContext(),list);
        resto_lv.setAdapter(listePlatItemAdapter);

        resto_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), RestaurantDetailPlatActivity.class);
                i.putExtra("plat",list.get(position));
                startActivity(i);
            }
        });

    }
}