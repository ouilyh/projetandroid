package bf.ouily.ribbala.vue.adpter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.model.Restaurant;
import bf.ouily.ribbala.model.User;

public class AdminListeAdapter extends BaseAdapter {

    private ArrayList<Restaurant> data = new ArrayList<Restaurant>();

    private LayoutInflater inflater;
    private Context context;



    public AdminListeAdapter(Context context,ArrayList<Restaurant> data) {
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if (convertView == null){
            holder  = new Holder();

            convertView = inflater.inflate(R.layout.restaurant_admin_adpter,null);

            holder.nomResto = (TextView) convertView.findViewById(R.id.resto_adpter_nom);
            holder.adresse = (TextView) convertView.findViewById(R.id.resto_adpter_adresse);
            holder.email = (TextView) convertView.findViewById(R.id.resto_adpter_email);
            holder.validiter = (TextView) convertView.findViewById(R.id.resto_adpter_validiter);
            holder.delBtn = (ImageView)convertView.findViewById(R.id.resto_adpter_delette);

            holder.delBtn.setTag(position);

            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        holder.nomResto.setText("Restaurant "+data.get(position).getNomRest());
        holder.email.setText(data.get(position).getEmail());
        holder.adresse.setText(data.get(position).getAdresse());

        if (UserControl.getInstance().getUser().getTypeUser() == 3) holder.validiter.setVisibility(View.VISIBLE);
        holder.delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (int)v.getTag();
                RestaurantControl.getInstance(context).delRestauant(i);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    private class Holder{
        TextView nomResto;
        TextView adresse;
        TextView email;
        TextView validiter;
        ImageView delBtn;
    }
}
