package bf.ouily.ribbala.vue.adpter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.Inflater;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.model.Commande;
import bf.ouily.ribbala.outil.Outils;

public class CommandeAdapter extends BaseAdapter {

    private ArrayList<Commande> commandes = new ArrayList<Commande>();
    private LayoutInflater inflater;

    public CommandeAdapter(Context context,ArrayList<Commande> commandes) {
        this.inflater = LayoutInflater.from(context);
        this.commandes = commandes;
    }

    @Override
    public int getCount() {
        return commandes.size();
    }

    @Override
    public Object getItem(int position) {
        return commandes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.commande_adapter,null);

            holder = new Holder();
            holder.date = convertView.findViewById(R.id.commade_date_tv);
            holder.nmbPlat = convertView.findViewById(R.id.commande_nmbre_plat);
            holder.somme = convertView.findViewById(R.id.commande_somme);
            holder.btn = convertView.findViewById(R.id.commade_btn_tv);
            holder.livraison = convertView.findViewById(R.id.commade_adapter_livraison);
            holder.etat = convertView.findViewById(R.id.commade_etat_tv);

            holder.btn.setTag(position);

            convertView.setTag(holder);

        }else {
            holder = (Holder)convertView.getTag();
        }

        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy 'à' hh:mm");
        holder.date.setText(formater.format(commandes.get(position).getDate()));
        holder.nmbPlat.setText(Outils.PlatCommandeSize(commandes.get(position).getPlatCommanders())+" Plats");
        holder.somme.setText(commandes.get(position).getSomme()+" Fcfa");

        if(commandes.get(position).getLivraison()){
            holder.livraison.setText("A livrer");
            holder.livraison.setTextColor(0xffff9800);
        }else {
            holder.livraison.setText("Pas de livraison");
        }

        if (commandes.get(position).isEtat()){
            holder.etat.setText("Prete");
            holder.etat.setVisibility(View.VISIBLE);
            holder.btn.setVisibility(View.GONE);
        }else if (commandes.get(position).isEtat2()){
            holder.etat.setText("Encour");
            holder.etat.setVisibility(View.VISIBLE);
            holder.btn.setVisibility(View.GONE);
        }

        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commandes.remove(position);
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    private class Holder{
        TextView date;
        TextView nmbPlat;
        TextView somme;
        TextView btn;
        TextView livraison;
        TextView etat;


    }
}
