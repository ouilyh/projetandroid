package bf.ouily.ribbala.vue.adpter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatControl;
import bf.ouily.ribbala.controlleur.RestaurantControl;
import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.vue.activite.EditPlatActivity;

public class ListePlatItemAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Plat> data;
    private int type = 0;
    private Boolean resto = false;

    Context context;

    public ListePlatItemAdapter(Context context, ArrayList<Plat> data){
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
    }
    public ListePlatItemAdapter(Context context, ArrayList<Plat> data,int type,Boolean resto){
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.type = type;
        this.resto = resto;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if ( convertView == null){

            convertView = inflater.inflate(R.layout.plat_item,null);

            holder = new Holder();
            holder.item_name = (TextView) convertView.findViewById(R.id.item_plat_name);
            holder.item_type = (TextView) convertView.findViewById(R.id.item_plat_type);
            holder.item_prix = (TextView) convertView.findViewById(R.id.item_plat_prix);
            holder.item_resto = (TextView) convertView.findViewById(R.id.item_plat_resto);
            holder.item_del_ll = (LinearLayout) convertView.findViewById(R.id.plat_item_delete);
            holder.item_del_btn = convertView.findViewById(R.id.plat_item_delete_btn);
            holder.item_edit_btn = convertView.findViewById(R.id.plat_item_edit_btn);

            holder.item_edit_btn.setTag(position);
            holder.item_del_btn.setTag(position);

            convertView.setTag(holder);
        }else {
            holder = (Holder)convertView.getTag();
        }

//        if (resto){
//
//            if (data.get(position).getType() == type && RestaurantControl.getInstance().getUser().getId() == data.get(position).getUser().getId()){
//
//                holder.item_name.setText(data.get(position).getNomPlat());
//                holder.item_prix.setText(""+data.get(position).getPrix());
//
//                if(data.get(position).getType() == 1)
//                    holder.item_type.setText("Boisson");
//
//                if(data.get(position).getType() == 2)
//                    holder.item_type.setText("Desert");
//            }
//
//        }else {
//
//            if (data.get(position).getType() == type ){
//
//                holder.item_name.setText(data.get(position).getNomPlat());
//                holder.item_prix.setText(""+data.get(position).getPrix());
//
//                if(type == 1)
//                    holder.item_type.setText("Boisson");
//
//                if(type == 2)
//                    holder.item_type.setText("Desert");
//            }
//
//        }

        holder.item_name.setText(data.get(position).getNomPlat());
        holder.item_prix.setText(""+data.get(position).getPrix());
        holder.item_resto.setText("Restaurant "+data.get(position).getRestaurant().getNomRest());
        if (UserControl.getInstance().getUser().getTypeUser() == 2)
            holder.item_del_ll.setVisibility(View.VISIBLE);

        if(data.get(position).getType() == 1)
            holder.item_type.setText("Boisson");

        if(data.get(position).getType() == 2)
            holder.item_type.setText("Desert");


        return convertView;
    }

    private class  Holder{
        TextView item_name;
        TextView item_type;
        TextView item_prix;
        TextView item_resto;
        LinearLayout item_del_ll;
        ImageView item_del_btn;
        ImageView item_edit_btn;
    }
}
