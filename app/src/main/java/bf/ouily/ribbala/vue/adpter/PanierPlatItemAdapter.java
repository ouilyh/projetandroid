package bf.ouily.ribbala.vue.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.PlatCommanderControl;
import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.model.AccessLocal;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.model.PlatCommander;
import bf.ouily.ribbala.vue.fragment.PanierFragment;

public class PanierPlatItemAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<PlatCommander> data;

    private PlatCommanderControl platCommanderControl = PlatCommanderControl.getInstance();
    private Context context;

    public PanierPlatItemAdapter(Context context, ArrayList<PlatCommander> data){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if ( convertView == null){

            convertView = inflater.inflate(R.layout.plat_panier_adpter,null);

            holder = new Holder();
            holder.item_name        = (TextView) convertView.findViewById(R.id.panier_plat_name);
            holder.item_type        = (TextView) convertView.findViewById(R.id.panier_plat_type);
            holder.item_quantite    = (TextView) convertView.findViewById(R.id.panier_item_quantite);
            holder.item_btn_add     = (TextView) convertView.findViewById(R.id.panier_tv_btn_add);
            holder.item_btn_moins   = (TextView) convertView.findViewById(R.id.panier_tv_btn_moins);
            holder.item_resto       = (TextView) convertView.findViewById(R.id.panier_resto);
            holder.item_compt       = convertView.findViewById(R.id.panier_compteur);
            holder.item_comm        = convertView.findViewById(R.id.resto_com_part);
            holder.item_btn_ok      = convertView.findViewById(R.id.panier_item_pret);
            holder.item_quantite2   = convertView.findViewById(R.id.panier_item_quantite2);
            holder.item_plat_pret   = convertView.findViewById(R.id.panier_item_pret);

            holder.item_btn_add.setTag(position);
            holder.item_btn_moins.setTag(position);
            holder.item_plat_pret.setTag(position);

            convertView.setTag(holder);
        }else {
            holder = (Holder)convertView.getTag();
        }

        holder.item_name.setText(data.get(position).getPlat().getNomPlat());
        holder.item_quantite.setText(""+data.get(position).getQuantite());

        if (UserControl.getInstance().getUser().getTypeUser() == 2){
            holder.item_compt.setVisibility(View.GONE);
            holder.item_comm.setVisibility(View.VISIBLE);
            holder.item_quantite2.setText(""+data.get(position).getQuantite());
            holder.item_resto.setText("Pour "+new AccessLocal(context).getClientDAO().findUserId(data.get(position).getCommande().getUser().getId()).getNomClient());
        }else {
            holder.item_comm.setVisibility(View.GONE);
            holder.item_compt.setVisibility(View.VISIBLE);
        }

        if (data.get(position).isEtat()){
            holder.item_plat_pret.setText("OK");
        }else {
            holder.item_plat_pret.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = (int)v.getTag();
                    data.get(i).setEtat(true);
                    new AccessLocal(context).getPlatCommaderDAO().update(data.get(i));

                    notifyDataSetChanged();
                }
            });
        }

        holder.item_btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = (int)v.getTag();
                int quantite = platCommanderControl.getPlatCommanders().get(pos).getQuantite();
                platCommanderControl.getPlatCommanders().get(pos).setQuantite(quantite+1);
                int somme = platCommanderControl.getSomme();
                somme += platCommanderControl.getPlatCommanders().get(pos).getPlat().getPrix();
                platCommanderControl.setSomme(somme);

                PanierFragment.updateValue();

                notifyDataSetChanged();

            }
        });

        holder.item_btn_moins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = (int)v.getTag();
                int quantite = platCommanderControl.getPlatCommanders().get(pos).getQuantite();
                platCommanderControl.getPlatCommanders().get(pos).setQuantite(quantite-1);
                int somme = platCommanderControl.getSomme();
                somme -= platCommanderControl.getPlatCommanders().get(pos).getPlat().getPrix();
                platCommanderControl.setSomme(somme);

                notifyDataSetChanged();

            }
        });


        if(data.get(position).getPlat().getType() == 1)
            holder.item_type.setText("Boisson");
        if(data.get(position).getPlat().getType() == 2)
            holder.item_type.setText("Desert");
        return convertView;
    }

    private  class  Holder{
        TextView item_name;
        TextView item_type;
        TextView item_quantite;
        TextView item_btn_moins;
        TextView item_btn_add;
        TextView item_resto;
        TextView item_btn_ok;
        TextView item_quantite2;
        TextView item_plat_pret;
        ConstraintLayout item_compt;
        ConstraintLayout item_comm;
    }
}
