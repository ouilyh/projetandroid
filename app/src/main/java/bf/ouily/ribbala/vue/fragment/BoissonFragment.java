package bf.ouily.ribbala.vue.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.vue.activite.DetailActivity;
import bf.ouily.ribbala.vue.adpter.ListePlatItemAdapter;
import bf.ouily.ribbala.R;


public class BoissonFragment extends Fragment {

    ArrayList<Plat> list = new ArrayList<Plat>();
    private ListView boissonL;
    public BoissonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_boisson, container, false);
        boissonL = view.findViewById(R.id.boisson_lv);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        ListePlatItemAdapter platItemAdapter = new ListePlatItemAdapter(getActivity(),list);

        boissonL.setAdapter(platItemAdapter);
        boissonL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), DetailActivity.class);
                i.putExtra("plat",list.get(position));
                i.putExtra("item_type","Boisson");
                startActivity(i);
            }
        });

    }

    public ArrayList<Plat> getArrayList() {
        return list;
    }

    public void setArrayList(ArrayList<Plat> arrayList) {
        this.list = arrayList;
    }
}