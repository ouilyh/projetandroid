package bf.ouily.ribbala.vue.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.model.Plat;
import bf.ouily.ribbala.vue.activite.DetailActivity;
import bf.ouily.ribbala.vue.adpter.ListePlatItemAdapter;


public class DesertFragment extends Fragment {

    ArrayList<Plat> list = new ArrayList<Plat>();
    private ListView desertL;

    public DesertFragment() {
        // Required empty public constructor
    }


    public static DesertFragment newInstance(String param1, String param2) {
        DesertFragment fragment = new DesertFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_desert, container, false);
        desertL = (ListView) view.findViewById(R.id.desert_lv);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ListePlatItemAdapter listePlatItemAdapter = new ListePlatItemAdapter(getActivity(),list);
        desertL.setAdapter(listePlatItemAdapter);

        desertL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), DetailActivity.class);
                i.putExtra("plat",list.get(position));
                i.putExtra("item_type","Boisson");
                startActivity(i);
            }
        });


    }

    public ArrayList<Plat> getList() {
        return list;
    }

    public void setList(ArrayList<Plat> list) {
        this.list = list;
    }
}