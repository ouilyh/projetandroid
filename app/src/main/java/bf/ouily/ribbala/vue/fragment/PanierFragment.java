package bf.ouily.ribbala.vue.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import bf.ouily.ribbala.R;
import bf.ouily.ribbala.controlleur.ClientControl;
import bf.ouily.ribbala.controlleur.CommandeControl;
import bf.ouily.ribbala.controlleur.PlatCommanderControl;
import bf.ouily.ribbala.controlleur.UserControl;
import bf.ouily.ribbala.model.Commande;
import bf.ouily.ribbala.model.PlatCommander;
import bf.ouily.ribbala.model.User;
import bf.ouily.ribbala.vue.activite.DetailActivity;
import bf.ouily.ribbala.vue.adpter.PanierPlatItemAdapter;
import bf.ouily.ribbala.vue.activite.PlatActivity2;


public class PanierFragment extends Fragment {

    ArrayList<PlatCommander> list = new ArrayList<PlatCommander>();

    private static TextView panier_nb_plat;
    private static TextView panier_total_prix;
    private ListView panierL;
    private static PlatCommanderControl platCommanderControl = PlatCommanderControl.getInstance();
    private CommandeControl commandeControl = CommandeControl.getInstance();

    //Layout
    private TextView panier_vide;
    private LinearLayout panier_detail;
    private Button annule_btn;
    private Button valide_btn;

    private static final int DIALOG_ALERT = 10;

    Dialog dialog;
    Dialog valideDialog;

    public PanierFragment() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_panier, container, false);
        panierL = view.findViewById(R.id.panier_lv);

        panier_nb_plat = view.findViewById(R.id.panier_nb_plat);
        panier_total_prix = view.findViewById(R.id.panier_total_prix);

        panier_vide = view.findViewById(R.id.panier_vide);
        panier_detail = view.findViewById(R.id.panier_detail);

        valide_btn = view.findViewById(R.id.panier_valide_btn);
        annule_btn = view.findViewById(R.id.panier_annule_btn);

        annule_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheAnnuleDialog();

            }
        });

        valide_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheValideDialog();
            }
        });
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        list = platCommanderControl.getPlatCommanders();
        if (platCommanderControl.getSomme() == 0){
            panier_vide.setVisibility(View.VISIBLE);
            panier_detail.setVisibility(View.GONE);
            panierL.setVisibility(View.GONE);
        }else {
            panierL.setVisibility(View.VISIBLE);
            panier_vide.setVisibility(View.GONE);
            panier_detail.setVisibility(View.VISIBLE);
            updateValue();
            if (list != null & list.size()!=0 ) {
                PanierPlatItemAdapter panierPlatItemAdapter = new PanierPlatItemAdapter(getActivity(), list);

                panierL.setAdapter(panierPlatItemAdapter);
                panierL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getActivity(), DetailActivity.class);
                        i.putExtra("plat", list.get(position).getPlat());
                        i.putExtra("item_type", "Boisson");
                        startActivity(i);
                    }
                });
            }else{

                PanierPlatItemAdapter panierPlatItemAdapter = new PanierPlatItemAdapter(getActivity(),null);
            }

        }

    }

    // Mettre les valeur du detail des commandes dans le panier
    public static void updateValue(){

        panier_total_prix.setText(platCommanderControl.getSomme()+"Fcfa");
        int size = 0;
        for(int i = 0; i < platCommanderControl.getPlatCommanders().size();i++)
            size+=platCommanderControl.getPlatCommanders().get(i).getQuantite();

        panier_nb_plat.setText(size+" Plats");
    }

    // Valider une commande

    private void valideCommande(){

        Commande commande = new Commande(new Date(),platCommanderControl.getPlatCommanders(),platCommanderControl.getSomme());
        commandeControl.addCommande(commande);

        PlatCommanderControl.changeIntance();
        PlatActivity2.updateBadgeValue();
        platCommanderControl = PlatCommanderControl.getInstance();
        list = platCommanderControl.getPlatCommanders();
        onResume();

        //reinitialisePatCommadeInstance();
    }

    private void reinitialisePatCommadeInstance(){

        platCommanderControl.setPlatCommanders(new ArrayList<PlatCommander>());
        platCommanderControl.setSomme(0);

    }

    //Boite d'alerte

    private void afficheAnnuleDialog(){

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.annule_panier_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        Button non = dialog.findViewById(R.id.panier_dialog_annul_btn);
        Button oui = dialog.findViewById(R.id.panier_dialog_valide_btn);

        non.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        oui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reinitialisePatCommadeInstance();
                onResume();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void afficheValideDialog(){

        valideDialog = new Dialog(getActivity());
        valideDialog.setContentView(R.layout.valide_panier_dialog);
        valideDialog.getWindow().getAttributes().windowAnimations = R.style.animation;
        valideDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        valideDialog.setCancelable(false);

        Button annuler = valideDialog.findViewById(R.id.panier_dialog_valide_annul_btn);
        Button commander = valideDialog.findViewById(R.id.panier_dialog_valide_commande_btn);
        CheckBox livraison = valideDialog.findViewById(R.id.livraison);

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valideDialog.dismiss();
            }
        });
        commander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Commande commande = new Commande();
                commande.setDate(new Date());
                commande.setPlatCommanders(platCommanderControl.getPlatCommanders());
                Log.d("Plat commder-+*******************************", "onClick: " + platCommanderControl.getPlatCommanders().size());
                commande.setSomme(platCommanderControl.getSomme());
                UserControl userControl = UserControl.getInstance();
                commande.setUser(userControl.getUser());

                if(livraison.isChecked())
                    commande.setLivraison(true);
                else
                    commande.setLivraison(false);

                commandeControl.setContext(getActivity());
                commandeControl.addCommande(commande);

                //Demarrer une nuvelle instance des plats commander
                PlatCommanderControl.changeIntance();
                PlatActivity2.updateBadgeValue();
                platCommanderControl = PlatCommanderControl.getInstance();
                list = platCommanderControl.getPlatCommanders();
                onResume();
                valideDialog.dismiss();
            }
        });

        valideDialog.show();
    }

}